package geoiplookup

import (
	"testing"
)

type teststruct struct {
	val int
}

func TestGetInputAsString_Test1(t *testing.T) {
	// first test
	input := teststruct{}
	input.val = 3
	res, err := GetInputAsString(input)
	if len(res) > 0 {
		t.Error("Expected len==0")
	}
	if err == nil {
		t.Error("expected !nil error")
	}
	// test with a string
	res, err = GetInputAsString("a test")
	if len(res) == 0 {
		t.Error("Expected len>0")
	}
	if err != nil {
		t.Error("expected nil error on string")
	}
	// test with an int
	res, err = GetInputAsString(3)
	if len(res) == 0 {
		t.Error("Expected len>0")
	}
	if err != nil {
		t.Error("expected nil error on int")
	}
	// test with a float
	res, err = GetInputAsString(3.14)
	if len(res) == 0 {
		t.Error("Expected len>0")
	}
	if err != nil {
		t.Error("expected nil error on float")
	}
	// test with a []string
	myarr := []string{"a message"}
	res, err = GetInputAsString(myarr)
	if len(res) == 0 {
		t.Error("Expected len>0")
	}
	if err != nil {
		t.Error("expected nil error on []string")
	}
	myarr = append(myarr, "another message")
	res, err = GetInputAsString(myarr)
	if len(res) > 0 {
		t.Error("Expected len==0")
	}
	if err == nil {
		t.Error("expected !nil error on []string")
	}
}
