package geoiplookup

// JoinResponses joins together multiple responses into one. In case of a tie, the last entry wins.
func JoinResponses(responses ...*GeoIpResponse) (result *GeoIpResponse) {
	count := 0
	result = &GeoIpResponse{
		Values: make(map[string]string),
	}
	for x := range responses {
		if responses[x] == nil {
			continue
		}
		count++
		if len(responses[x].CountryCode) > 0 {
			result.CountryCode = responses[x].CountryCode
		}
		if responses[x].City != nil {
			result.City = responses[x].City
		}
		if responses[x].Region != nil {
			result.Region = responses[x].Region
		}
		if responses[x].Geo != nil {
			result.Geo = responses[x].Geo
		}
		if responses[x].Values != nil {
			for y := range responses[x].Values {
				result.Values[y] = responses[x].Values[y]
			}
		}
	}
	if count == 0 {
		return nil
	}
	return
}
