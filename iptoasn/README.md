# iptoasn

[iptoasn](https://iptoasn.com) is a free service that publishes a list of IP addresses to country and its AS info.

This is a wrapper for [ip2asn](https://github.com/bradfitz/ip2asn) that implements the search function.