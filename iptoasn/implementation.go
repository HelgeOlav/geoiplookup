package iptoasn

import (
	"bitbucket.org/HelgeOlav/geoiplookup"
	_ "bitbucket.org/HelgeOlav/utils"
	"bitbucket.org/HelgeOlav/utils/filewatcher"
	"context"
	"errors"
	"github.com/bradfitz/ip2asn"
	"inet.af/netaddr"
	"log"
	"strconv"
	"sync"
	"time"
)

// database is our implementation containing all data we need
type database struct {
	mtx      sync.RWMutex // access lock on the Map below
	Map      *ip2asn.Map  // the database loaded into memory
	filename string       // name of the file we are using
}

// Search is an implementation of geoiplookup.GeoIpLookup - input is an IP address and the result is fetched from the database
func (d *database) Search(input interface{}) (result *geoiplookup.GeoIpResponse, err error) {
	ipString, err := geoiplookup.GetInputAsString(input)
	if err != nil {
		return nil, err
	}
	ip, err := netaddr.ParseIP(ipString)
	if err != nil {
		return nil, err
	}
	d.mtx.RLock()
	defer d.mtx.RUnlock()
	if d.Map == nil {
		return nil, errors.New("iptoasn not loaded")
	}
	as := d.Map.ASofIP(ip)
	if as == 0 {
		return nil, errors.New("did not find IP in database")
	}
	country := d.Map.ASCountry(as)
	asn := d.Map.ASName(as)
	result = &geoiplookup.GeoIpResponse{CountryCode: country}
	result.Values = make(map[string]string)
	result.Values["AS"] = strconv.Itoa(as)
	result.Values["AS_NAME"] = asn
	return result, nil
}

// loaddb loads the database into memory
func (d *database) loaddb() error {
	newMap, err := ip2asn.OpenFile(d.filename)
	if err == nil {
		d.mtx.Lock()
		d.Map = newMap
		d.mtx.Unlock()
	}
	return err
}

// filewatchercallback handles file updates from filewatcher
func (d *database) filewatchercallback(fn string) {
	// if called without a filename it is to init code
	if fn == "" {
		_, err := filewatcher.NewFileWatcher(context.Background(), d.filename, d.filewatchercallback, 10*time.Second)
		if err != nil {
			log.Println(err)
		}
		return
	}
	// the file has changed
	err := d.loaddb()
	if err != nil {
		log.Println("Reload failed:", err.Error())
	}
}

// New returns a new object and loads the database
func New(filename string) (geoiplookup.GeoIpLookup, error) {
	db := database{filename: filename}
	err := db.loaddb()
	db.filewatchercallback("")
	return &db, err
}
