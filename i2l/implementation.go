package i2l

import (
	"context"
	"log"
	"sync"
	"time"

	"bitbucket.org/HelgeOlav/utils/filewatcher"
	"github.com/ip2location/ip2proxy-go"

	"bitbucket.org/HelgeOlav/geoiplookup"
	"github.com/ip2location/ip2location-go/v9"
)

// IP2Location contains all that is needed to to database lookups
type IP2Location struct {
	mtx             sync.RWMutex    // mutex for ip2locationdb and ip2proxydb
	fileIp2location string          // filename of the ip2location database
	fileIp2proxy    string          // name of the proxy database
	ip2locationdb   *ip2location.DB // the ip2location database that we have opened
	ip2proxydb      *ip2proxy.DB    // the ip2proxy database that we have opened
}

const (
	Ip2NotSupported      = "This parameter is unavailable for selected data file. Please upgrade the data file." // The error printed on each property that is not supported in the database
	Ip2IpLookupError     = "Invalid IP address."                                                                 // Error in fields when there is an error in the IP address
	Ip2ProxyNotSupported = "NOT SUPPORTED"                                                                       // not supported fields on ip2proxy
)

// Search is an implementation of geoiplookup.GeoIpLookup - input is an IP address and the result is fetched from the database
func (d *IP2Location) Search(input interface{}) (result *geoiplookup.GeoIpResponse, err error) {
	// ip2location
	db1, err := d.SearchIP2Location(input)
	if err != nil {
		return
	}
	if db1 != nil {
		result = db1
	}
	// ip2proxy
	db2, err := d.SearchIP2proxy(input)
	if err != nil {
		return
	}
	return geoiplookup.JoinResponses(db2, db1), nil
}

func (d *IP2Location) SearchIP2proxy(input interface{}) (*geoiplookup.GeoIpResponse, error) {
	if d.ip2proxydb == nil {
		return nil, nil
	}
	ip, err := geoiplookup.GetInputAsString(input)
	if err != nil {
		return nil, err
	}
	// fetch record
	d.mtx.RLock()
	result, err := d.ip2proxydb.GetAll(ip)
	d.mtx.RUnlock()
	if err != nil {
		return nil, err
	}
	// make the result
	response := geoiplookup.GeoIpResponse{}
	response.Values = make(map[string]string)
	for x := range result {
		switch x {
		case "CountryShort":
			response.CountryCode = result["CountryShort"]
		case "Region":
			r := result["Region"]
			if r != Ip2ProxyNotSupported {
				response.Region = &r
			}
		case "City":
			r := result["City"]
			if r != Ip2ProxyNotSupported {
				response.City = &r
			}
		default:
			if result[x] != Ip2ProxyNotSupported {
				response.Values[x] = result[x]
			}
		}
	}

	return &response, nil
}

// SearchIP2Location searches ip2location database
func (d *IP2Location) SearchIP2Location(input interface{}) (*geoiplookup.GeoIpResponse, error) {
	if d.ip2locationdb == nil {
		return nil, nil
	}
	var ip2dbresult ip2location.IP2Locationrecord
	// try to get IP
	ip, err := geoiplookup.GetInputAsString(input)
	if err != nil {
		return nil, err
	}
	// fetch record
	d.mtx.RLock()
	ip2dbresult, err = d.ip2locationdb.Get_all(ip)
	d.mtx.RUnlock()
	if err != nil {
		return nil, err
	}
	// and return response
	result := geoiplookup.GeoIpResponse{}
	result.CountryCode = ip2dbresult.Country_short
	if ip2dbresult.City != Ip2NotSupported {
		result.City = &ip2dbresult.City
	}
	if ip2dbresult.Region != Ip2NotSupported {
		result.Region = &ip2dbresult.Region
	}
	if ip2dbresult.Latitude != 0 && ip2dbresult.Longitude != 0 {
		result.Geo = &geoiplookup.Location{Latitude: float64(ip2dbresult.Latitude), Longitude: float64(ip2dbresult.Longitude)}
	}
	result.Values = make(map[string]string)
	if ip2dbresult.Isp != Ip2NotSupported {
		result.Values["ISP"] = ip2dbresult.Isp
	}
	return &result, nil
}

// NewI2L opens the database specified by filename and returns an object. If a database is empty then it is not used.
func NewI2L(ip2locationdb, ip2proxydb string) (*IP2Location, error) {
	i2l := IP2Location{
		fileIp2location: ip2locationdb,
		fileIp2proxy:    ip2proxydb,
	}
	if len(ip2locationdb) > 0 {
		db, err := ip2location.OpenDB(ip2locationdb)
		if err != nil {
			return nil, err
		}
		i2l.ip2locationdb = db
		go i2l.filenotifyip2l("")
	}
	if len(ip2proxydb) > 0 {
		proxydb, err := ip2proxy.OpenDB(ip2proxydb)
		if err != nil {
			return nil, err
		}
		i2l.ip2proxydb = proxydb
	}
	return &i2l, nil
}

// Close closes the file, no searches can be done after this point
func (d *IP2Location) Close() {
	d.mtx.Lock()
	d.ip2locationdb.Close()
	d.ip2locationdb = nil
	d.mtx.Unlock()
}

// filenotifyip2l handles changes to the underlying file for ip2location
func (d *IP2Location) filenotifyip2l(name string) {
	// set up new watcher
	if name == "" {
		_, err := filewatcher.NewFileWatcher(context.Background(), d.fileIp2location, d.filenotifyip2l, 10*time.Second)
		if err != nil {
			log.Println(err)
		}
		return
	}
	// ok, the file has changed
	log.Println("Reloading file", name)
	db, err := ip2location.OpenDB(name)
	if err != nil {
		log.Println(err)
		return
	}
	d.mtx.Lock()
	olddb := d.ip2locationdb
	d.ip2locationdb = db
	d.mtx.Unlock()
	olddb.Close()
}

// filenotifyip2p handles changes to the underlying file for ip2proxy
func (d *IP2Location) filenotifyip2p(name string) {
	// set up new watcher
	if name == "" {
		_, err := filewatcher.NewFileWatcher(context.Background(), d.fileIp2proxy, d.filenotifyip2p, time.Second*10)
		if err != nil {
			log.Println(err)
		}
		return
	}
	// ok, the file has changed
	log.Println("Reloading file", name)
	db, err := ip2proxy.OpenDB(name)
	if err != nil {
		log.Println(err)
		return
	}
	d.mtx.Lock()
	olddb := d.ip2proxydb
	d.ip2proxydb = db
	d.mtx.Unlock()
	olddb.Close()
}
