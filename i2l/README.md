# ip2location - search interface

This is a reference implementation of the server side of geoiplookup using the ip2location binary database format and its golang library.

ip2proxy is also supported and can be combined to get one result.

The database is automatically reloaded on changes.

A server that uses this implementation is [sample-i2l](../examples/sample-i2l). Look into the code and see how it is used.

Client code is here: [grpc-client](../examples/grpc-client). 