package geoiplookup

import "bitbucket.org/HelgeOlav/utils/version"

const Version = "0.0.2.2"

func init() {
	version.AddModule(version.ModuleVersion{Name: "bitbucket.org/HelgeOlav/geoiplookup", Version: Version})
}
