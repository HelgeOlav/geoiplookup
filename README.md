# Introduction

geoiplookup provides an interface for doing lookups to backend databases.
There is one provider included (ip2location) to give an example on how this can be implemented.

The goal is to make an abstraction layer between the backend service ("GeoIP database") and the frontend.

This library has two components:

1. A local API (an interface) to do lookups on your computer.
2. A gRPC specification to do lookups remotely.