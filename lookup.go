package geoiplookup

import (
	"errors"
	"fmt"
	"reflect"
)

// GeoIpLookup is the interface that implement searches in any given database locally. gRPC has its own methods.
type GeoIpLookup interface {
	// Search searches the implementation for a match on input. String is the only mandatory input type to be supported.
	// Both IPv4 and IPv6 addresses has to be supported.
	// Result can be either nil or an empty Result{} in case there is no match. If an error is returned the Result can not be trusted.
	Search(input interface{}) (*GeoIpResponse, error)
}

// GetInputAsString is a helper function that parses the input and returns the result as an string, or an error if it could not be converted to a string
func GetInputAsString(input interface{}) (result string, err error) {
	switch v := input.(type) {
	case string:
		result = v
	case *string:
		result = *v
	case fmt.Stringer:
		result = v.String()
	case int, int8, uint8, int32, int64, uint16, uint32, uint64, float32, float64:
		result = fmt.Sprintf("%v", input)
	case []string:
		if len(v) == 1 {
			result = v[0]
		} else if len(v) != 1 {
			err = errors.New("[]string has not exactly one element")
		}
	default:
		err = fmt.Errorf("unsupported input %v", reflect.TypeOf(input))
	}
	return
}
