@echo off
setlocal
git rev-parse HEAD > hash.txt
date /t > date.txt
set /p GIT=<hash.txt
set /p STAMP=<date.txt

go build -ldflags="-X bitbucket.org/HelgeOlav/utils/version.BUILDTIME=%STAMP% -X bitbucket.org/HelgeOlav/utils/version.GITCOMMIT=%GIT%" -o grpc-client.exe .\examples\grpc-client
go build -ldflags="-X bitbucket.org/HelgeOlav/utils/version.BUILDTIME=%STAMP% -X bitbucket.org/HelgeOlav/utils/version.GITCOMMIT=%GIT%" -o i2l-server.exe .\examples\sample-i2l
go build -ldflags="-X bitbucket.org/HelgeOlav/utils/version.BUILDTIME=%STAMP% -X bitbucket.org/HelgeOlav/utils/version.GITCOMMIT=%GIT%" -o lookup-proxy.exe .\examples\lookup-proxy
go build -ldflags="-X bitbucket.org/HelgeOlav/utils/version.BUILDTIME=%STAMP% -X bitbucket.org/HelgeOlav/utils/version.GITCOMMIT=%GIT%" -o sample-ip2asn.exe .\examples\sample-ip2asn
go build -ldflags="-X bitbucket.org/HelgeOlav/utils/version.BUILDTIME=%STAMP% -X bitbucket.org/HelgeOlav/utils/version.GITCOMMIT=%GIT%" -o bulk-lookup.exe .\examples\bulk-lookup

set GOARCH=amd64
set GOOS=linux
go build -ldflags="-X bitbucket.org/HelgeOlav/utils/version.BUILDTIME=%STAMP% -X bitbucket.org/HelgeOlav/utils/version.GITCOMMIT=%GIT%" -o grpc-client .\examples\grpc-client
go build -ldflags="-X bitbucket.org/HelgeOlav/utils/version.BUILDTIME=%STAMP% -X bitbucket.org/HelgeOlav/utils/version.GITCOMMIT=%GIT%" -o i2l-server .\examples\sample-i2l
go build -ldflags="-X bitbucket.org/HelgeOlav/utils/version.BUILDTIME=%STAMP% -X bitbucket.org/HelgeOlav/utils/version.GITCOMMIT=%GIT%" -o lookup-proxy .\examples\lookup-proxy
go build -ldflags="-X bitbucket.org/HelgeOlav/utils/version.BUILDTIME=%STAMP% -X bitbucket.org/HelgeOlav/utils/version.GITCOMMIT=%GIT%" -o sample-ip2asn .\examples\sample-ip2asn
go build -ldflags="-X bitbucket.org/HelgeOlav/utils/version.BUILDTIME=%STAMP% -X bitbucket.org/HelgeOlav/utils/version.GITCOMMIT=%GIT%" -o bulk-lookup .\examples\bulk-lookup

endlocal
