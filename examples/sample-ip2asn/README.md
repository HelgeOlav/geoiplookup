# sample-i2l

This is an example on how to run a server based on a [ip2asn](https://iptoasn.com) database.

This server serves both HTTP and gRPC requests.

To use a HTTP request call it like this

```shell
curl http://localhost:8080/1.1.1.1
```

To get a result like this back:

```json
{"country_code":"US","city":"Los Angeles","region":"California"}
```
## Syntax

```shell
sample-ip2asn -db path-to-db-file -http 8080 -grpc 8081
```