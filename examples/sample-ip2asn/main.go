package main

import (
	"bitbucket.org/HelgeOlav/geoiplookup/iptoasn"
	"bitbucket.org/HelgeOlav/utils/version"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"strconv"

	pb "bitbucket.org/HelgeOlav/geoiplookup"
	"google.golang.org/grpc"
)

type server struct {
	pb.UnimplementedGeoIpLookupServer                // needed by gRPC
	db                                pb.GeoIpLookup // the lookup service
}

var (
	dbFile   = flag.String("db", "ip2asn-combined.tsv", "ip2asn tsv file")
	httpPort = flag.Int("http", 8080, "HTTP listen port")
	grpcPort = flag.Int("grpc", 8081, "gRPC listen port")
)

func newServer(fn string) (*server, error) {
	var err error
	s := server{}
	s.db, err = iptoasn.New(fn)
	return &s, err
}

// webhandles servers HTTP requests, looking for IP in URL
func (s *server) webhandler(w http.ResponseWriter, r *http.Request) {
	res, err := s.db.Search(r.URL.Path[1:])
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprint(w, err.Error())
		return
	}
	bytes, _ := json.Marshal(&res)
	w.Header().Set("Content-Type", "application/json")
	w.Write(bytes)
}

// Lookup is the gRPC handler
func (s *server) Lookup(ctx context.Context, r *pb.GeoIpRequest) (*pb.GeoIpResponse, error) {
	resp, err := s.db.Search(r.Ip)
	return resp, err
}

func init() {
	version.NAME = "sample-ip2asn"
	version.VERSION = pb.Version
}

func main() {
	log.Println(version.Get().LongString())
	flag.Parse()
	conn, err := newServer(*dbFile)
	if err != nil {
		fmt.Println(err)
		return
	}
	if *grpcPort > 0 {
		go start_grpc_server(conn)
	}
	if *httpPort > 0 {
		http.HandleFunc("/", conn.webhandler)
		log.Println("Starting webserver on port", *httpPort)
		http.ListenAndServe(":"+strconv.Itoa(*httpPort), nil)
	}
	// wait forever if we run any service
	if *httpPort != 0 || *grpcPort != 0 {
		c := make(chan struct{})
		<-c
	}
}

func start_grpc_server(server *server) {
	s := grpc.NewServer()
	pb.RegisterGeoIpLookupServer(s, server)
	listen := ":" + strconv.Itoa(*grpcPort)
	list, err := net.Listen("tcp", listen)
	if err != nil {
		log.Println(err)
		return
	}
	log.Println("Starting gRPC on port", *grpcPort)
	log.Println(s.Serve(list))
}
