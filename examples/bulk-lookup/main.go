package main

import (
	pb "bitbucket.org/HelgeOlav/geoiplookup"
	"bitbucket.org/HelgeOlav/utils"
	"bitbucket.org/HelgeOlav/utils/version"
	"context"
	"encoding/csv"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"google.golang.org/grpc"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"sync"
	"time"
)

func init() {
	version.NAME = "grpc-client"
	version.VERSION = pb.Version
}

var (
	connString = flag.String("server", "localhost:8081", "connection to server")
	reqTimeout = flag.Uint("timeout", 3, "timeout in seconds")
	inputFile  = flag.String("in", "", "input filename")
	csvFile    = flag.String("csv", "", "output CSV file")
	jsonFile   = flag.String("json", "", "output JSON file")
	workers    = flag.Uint("workers", 10, "number of concurrent requests")
	csvFields  = flag.String("csvfields", "", "fields to add to CSV")
)

const lookupKey = "lookup_ip"

func main() {
	ver := version.Get()
	log.Println(ver.LongString())
	flag.Parse()
	if len(*inputFile) == 0 {
		flag.PrintDefaults()
		return
	}
	conn, err := grpc.Dial(*connString, grpc.WithInsecure())
	if err != nil {
		fmt.Println(err)
		return
	}
	c := pb.NewGeoIpLookupClient(conn)
	inputs, err := utils.ReadLines(*inputFile, true)
	if err != nil {
		fmt.Println(err)
		return
	}
	log.Println("starting on lookups")
	result, err := DoLookup(inputs, c, *workers)
	if err != nil {
		log.Println(err)
		return
	}
	log.Printf("done on lookups, got %d responses from %d requests\n", len(*result), len(inputs))
	if len(*csvFile) > 0 {
		err = SaveAsCsv(*csvFile, result)
		if err != nil {
			log.Println(err)
		}
	}
	if len(*jsonFile) > 0 {
		err = SaveAsJson(*jsonFile, result)
		if err != nil {
			log.Println(err)
		}
	}
	log.Println("all done")
}

// DoLookup will process line for line using goroutines
func DoLookup(inputIP []string, conn pb.GeoIpLookupClient, workers uint) (*[]pb.GeoIpResponse, error) {
	mainctx, maincancel := context.WithCancel(context.Background())
	defer maincancel()
	respCh := make(chan *pb.GeoIpResponse, 10) // channel to save responses to array
	reqCh := make(chan string, workers+1)      // channel to send requests to (input string for lookup)
	readerCh := make(chan *[]pb.GeoIpResponse) // channel that passes the final result back when everything is done
	// goroutine to receive responses on
	go func() {
		var result []pb.GeoIpResponse
		ticker := time.NewTicker(10 * time.Second)
		defer ticker.Stop()
		for {
			select {
			case <-ticker.C:
				log.Println("loop:", len(result), "entries processed")
			case msg := <-respCh:
				if msg == nil {
					log.Println("loop exiting")
					readerCh <- &result
					return
				}
				result = append(result, *msg)
			}
		}
	}()
	// launch workers
	wg := sync.WaitGroup{}
	for x := 0; x < int(workers); x++ {
		wg.Add(1)
		go func(index int) {
			defer wg.Done()
			log.Println("Worker", index, "started")
			for {
				select {
				case input := <-reqCh:
					if len(input) == 0 {
						log.Println("Worker", index, "stopped")
						return
					}
					req := pb.GeoIpRequest{Ip: input}
					ctx, cancel := context.WithTimeout(mainctx, time.Duration(*reqTimeout)*time.Second)
					resp, err := conn.Lookup(ctx, &req)
					cancel()
					if err != nil {
						log.Printf("Error on lookup (worker %d) for %s: %s\n", index, req.Ip, err)
						continue
					}
					if resp == nil {
						log.Printf("Error on lookup (worker %d) - got nil response but no error\n", index)
						continue
					}
					if resp.Values == nil {
						resp.Values = make(map[string]string)
					}
					resp.Values[lookupKey] = req.Ip
					respCh <- resp
				}
			}
		}(x)
	}
	// process all inputs
	for x := range inputIP {
		reqCh <- inputIP[x]
	}
	log.Println("all ips sent to queue")
	close(reqCh)
	wg.Wait()
	close(respCh)
	log.Println("waiting for loop to finish")
	result := <-readerCh
	return result, nil
}

// SaveAsJson saves the result av JSON to disk
func SaveAsJson(fileName string, data *[]pb.GeoIpResponse) error {
	if data == nil || len(*data) == 0 {
		return errors.New("No input data for JSON")
	}
	bytes, err := json.MarshalIndent(data, "", " ")
	if err != nil {
		return err
	}
	return ioutil.WriteFile(fileName, bytes, 0644)
}

// SaveAsCsv saves the resulting output as CSV file - only a few fields are included here
func SaveAsCsv(fileName string, data *[]pb.GeoIpResponse) error {
	if data == nil || len(*data) == 0 {
		return errors.New("No input data for csv")
	}
	file, err := os.Create(fileName)
	if err != nil {
		return err
	}
	w := csv.NewWriter(file)
	var fields []string
	if len(*csvFields) == 0 {
		fields = (*data)[0].GetFields()
	} else {
		fields = strings.Split(*csvFields, ",")
	}
	_ = w.Write(fields)
	for x := range *data {
		var o []string
		for key := range fields {
			res := (*data)[x].GetFieldValue(fields[key])
			if res == nil {
				o = append(o, "")
			} else {
				o = append(o, *res)
			}
		}
		err = w.Write(o)
		if err != nil {
			log.Println(err)
		}
	}
	w.Flush()
	return file.Close()
}
