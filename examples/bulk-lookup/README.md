# bulk-lookup

This tool will allow you to specify a file as input and do a bulk lookup on each line that is an IP address.

Tweak -timeout and -workers to maximize throughput.