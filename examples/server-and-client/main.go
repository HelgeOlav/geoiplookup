package main

import (
	"bitbucket.org/HelgeOlav/utils/version"
	"context"
	"fmt"
	"net"
	"strconv"
	"sync"
	"sync/atomic"
	"time"

	pb "bitbucket.org/HelgeOlav/geoiplookup"
	"google.golang.org/grpc"
)

func init() {
	version.NAME = "server-and-client"
	version.VERSION = pb.Version
}

func main() {
	fmt.Println(version.Get().LongString())
	go start_server()
	time.Sleep(time.Second * 1)
	do_client_stuff()
}

type server struct {
	pb.UnimplementedGeoIpLookupServer
}

const listenPort = 9999 // port for server
const Country = "NO"

func (s *server) Lookup(ctx context.Context, r *pb.GeoIpRequest) (*pb.GeoIpResponse, error) {
	delay, err := strconv.Atoi(r.Ip)
	if err == nil && delay > 0 {
		fmt.Println("server: got request, delay time", delay)
		timer := time.NewTimer(time.Second * time.Duration(delay))
		select {
		case <-ctx.Done():
			fmt.Println("server context cancelled", ctx.Err())
			return nil, ctx.Err()
		case <-timer.C:
			break
		}
	}
	resp := pb.GeoIpResponse{}
	resp.CountryCode = Country
	city := "Oslo"
	resp.City = &city
	resp.Values = make(map[string]string)
	resp.Values["message"] = "Hello World!"
	resp.Values["year"] = "2021"
	return &resp, nil
}

func start_server() {
	s := grpc.NewServer()
	pb.RegisterGeoIpLookupServer(s, &server{})
	listen := ":" + strconv.Itoa(listenPort)
	list, err := net.Listen("tcp", listen)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(s.Serve(list))
}

func do_client_stuff() {
	dial := "localhost:" + strconv.Itoa(listenPort)
	conn, err := grpc.Dial(dial, grpc.WithInsecure())
	if err != nil {
		fmt.Println(err)
		return
	}
	defer conn.Close()
	// now try to get a response
	c := pb.NewGeoIpLookupClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	req := pb.GeoIpRequest{Ip: "3"}
	r, err := c.Lookup(ctx, &req)
	if err != nil {
		fmt.Println("client: ", err)
	} else {
		fmt.Println(r, r.Values)
	}
	// do some performance testing
	var numErrors uint32
	var numOk uint32
	var wg sync.WaitGroup
	req.Ip = "1.2.3.4"
	fmt.Println("Doing testing")
	time.Sleep(time.Second)
	for iterations := 20000; iterations > 0; iterations-- {
		wg.Add(1)
		go func() {
			var delay int
			if iterations == 100 {
				delay = 30
				req.Ip = "10"
			} else {
				delay = 1
				req.Ip = "1.2.3.4"
			}
			ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(delay))
			resp, err := c.Lookup(ctx, &req)
			if err == nil && resp.CountryCode == Country {
				atomic.AddUint32(&numOk, 1)
			} else {
				atomic.AddUint32(&numErrors, 1)
			}
			cancel()
			wg.Done()
		}()
	}
	wg.Wait()
	fmt.Println(numOk, numErrors)
}
