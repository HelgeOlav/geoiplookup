package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"strconv"

	_ "bitbucket.org/HelgeOlav/utils"
	"bitbucket.org/HelgeOlav/utils/version"

	pb "bitbucket.org/HelgeOlav/geoiplookup"
	"bitbucket.org/HelgeOlav/geoiplookup/i2l"
	"google.golang.org/grpc"
)

type server struct {
	pb.UnimplementedGeoIpLookupServer                  // needed by gRPC
	db                                *i2l.IP2Location // used for db lookups
}

var (
	dbFile    = flag.String("db", "IP2LOCATION-LITE-DB5.BIN", "ip2location binary file")
	proxyFile = flag.String("proxy", "", "ip2proxy binary file")
	httpPort  = flag.Int("http", 8080, "HTTP listen port")
	grpcPort  = flag.Int("grpc", 8081, "gRPC listen port")
)

func newServer() (*server, error) {
	var err error
	s := server{}
	s.db, err = i2l.NewI2L(*dbFile, *proxyFile)
	return &s, err
}

// webhandles servers HTTP requests, looking for IP in URL
func (s *server) webhandler(w http.ResponseWriter, r *http.Request) {
	res, err := s.db.Search(r.URL.Path[1:])
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprint(w, err.Error())
		return
	}
	if res.CountryCode == i2l.Ip2IpLookupError {
		w.WriteHeader(http.StatusBadRequest)
	}
	bytes, _ := json.Marshal(&res)
	w.Header().Set("Content-Type", "application/json")
	w.Write(bytes)
}

// Lookup is the gRPC handler
func (s *server) Lookup(ctx context.Context, r *pb.GeoIpRequest) (*pb.GeoIpResponse, error) {
	resp, err := s.db.Search(r.Ip)
	if err == nil {
		if resp.CountryCode == i2l.Ip2IpLookupError {
			err = fmt.Errorf("%s is not an IP address", r.Ip)
		}
	}
	return resp, err
}

func init() {
	version.NAME = "sample-i2l"
	version.VERSION = pb.Version
}

func main() {
	log.Println(version.Get().LongString())
	flag.Parse()
	conn, err := newServer()
	if err != nil {
		fmt.Println(err)
		return
	}
	if *grpcPort > 0 {
		go start_grpc_server(conn)
	}
	if *httpPort > 0 {
		http.HandleFunc("/", conn.webhandler)
		log.Println("Starting webserver on port", *httpPort)
		http.ListenAndServe(":"+strconv.Itoa(*httpPort), nil)
	}
	// wait forever if we run any service
	if *httpPort != 0 || *grpcPort != 0 {
		c := make(chan struct{})
		<-c
	}
}

func start_grpc_server(server *server) {
	s := grpc.NewServer()
	pb.RegisterGeoIpLookupServer(s, server)
	listen := ":" + strconv.Itoa(*grpcPort)
	list, err := net.Listen("tcp", listen)
	if err != nil {
		log.Println(err)
		return
	}
	log.Println("Starting gRPC on port", *grpcPort)
	log.Println(s.Serve(list))
}
