package main

import (
	pb "bitbucket.org/HelgeOlav/geoiplookup"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
)

// webhandles servers HTTP requests, looking for IP in URL
func (s *Config) webhandler(w http.ResponseWriter, r *http.Request) {
	isip := pb.IsIP(r.URL.Path[1:])
	if !isip {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "%s is not an IP address", r.URL.Path[1:])
		return
	}
	ipreq := pb.GeoIpRequest{Ip: r.URL.Path[1:]}
	resp, err := s.Lookup(context.TODO(), &ipreq)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprint(w, err.Error())
		return
	}
	bytes, _ := json.Marshal(&resp)
	w.Header().Set("Content-Type", "application/json")
	w.Write(bytes)
}

func (c *Config) start_http_server() {
	http.HandleFunc("/", c.webhandler)
	log.Println("Starting webserver on port", c.HttpPort)
	http.ListenAndServe(":"+strconv.Itoa(c.HttpPort), nil)
}
