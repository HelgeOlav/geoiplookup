# lookup-proxy

This program is an example on how to proxy queries using gRPC. It will both combine from different sources and proxy requests.

Launch the server, specifying the backend server and what ports to listen on. Look at [config.json](config.json) to see how to configure the server.

```lookup-proxy -config config.json```