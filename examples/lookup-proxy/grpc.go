package main

import (
	pb "bitbucket.org/HelgeOlav/geoiplookup"
	"context"
	"fmt"
	"google.golang.org/grpc"
	"log"
	"net"
	"strconv"
	"sync"
	"time"
)

// Init inits the Backend
func (b *Backend) Init() (err error) {
	if len(b.Name) == 0 {
		b.Name = b.Server
	}
	log.Println("Init", b.Name)
	b.conn, err = grpc.Dial(b.Server, grpc.WithInsecure())
	return
}

// start_grpc_server launches the gRPC server to handle requests
func (c *Config) start_grpc_server() {
	s := grpc.NewServer()
	pb.RegisterGeoIpLookupServer(s, c)
	listen := ":" + strconv.Itoa(c.GrpcPort)
	list, err := net.Listen("tcp", listen)
	if err != nil {
		log.Println(err)
		return
	}
	log.Println("Starting gRPC on port", c.GrpcPort)
	log.Println(s.Serve(list))
}

// Lookup looks up in all backends and combines the result
func (c *Config) Lookup(ctx context.Context, r *pb.GeoIpRequest) (*pb.GeoIpResponse, error) {
	if !pb.IsIP(r.Ip) {
		return nil, fmt.Errorf("%s is not IP address", r.Ip)
	}
	newctx, cancel := context.WithTimeout(ctx, time.Duration(c.Timeout)*time.Second)
	defer cancel()
	wg := sync.WaitGroup{}
	numBackends := len(c.Backends)
	result := make([]*pb.GeoIpResponse, numBackends)
	errs := make([]error, numBackends)
	for x := range c.Backends {
		wg.Add(1)
		go func(x int) {
			client := pb.NewGeoIpLookupClient(c.Backends[x].conn)
			result[x], errs[x] = client.Lookup(newctx, r)
			wg.Done()
		}(x)
	}
	wg.Wait()
	// now check for any errors that we will not ignore
	for x := range c.Backends {
		if errs[x] != nil && !c.Backends[x].IgnoreErrors {
			return nil, errs[x]
		}
	}
	// now combine result and return
	return pb.JoinResponses(result...), nil
}
