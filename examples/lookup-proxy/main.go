package main

import (
	"bitbucket.org/HelgeOlav/geoiplookup"
	"bitbucket.org/HelgeOlav/utils"
	"bitbucket.org/HelgeOlav/utils/version"
	"flag"
	"google.golang.org/grpc"
	"log"
)

func init() {
	version.VERSION = geoiplookup.Version
	version.NAME = "lookup-proxy"
}

// Config is the configuration loaded from file
type Config struct {
	geoiplookup.UnimplementedGeoIpLookupServer           // needed by gRPC
	HttpPort                                   int       `json:"http_port"` // HTTP listen port
	GrpcPort                                   int       `json:"grpc_port"` // gRPC listen port
	Backends                                   []Backend `json:"backends"`  // configured backends
	Timeout                                    uint      `json:"timeout"`   // timeout for remote calls
}

// Backend represents a backend server
type Backend struct {
	Server       string           `json:"server"`        // host:port for the gRPC backend
	Name         string           `json:"name"`          // optional name of connection
	IgnoreErrors bool             `json:"ignore_errors"` // if true we will ignore errors on this output
	conn         *grpc.ClientConn // connection to the server
}

var (
	configFile = flag.String("config", "config.json", "configuration file")
)

func main() {
	ver := version.Get()
	flag.Parse()
	log.Println(ver.LongString())
	var config Config
	err := utils.ReadJsonFile(*configFile, &config)
	if err != nil {
		log.Println(err)
		return
	}
	if config.Timeout == 0 {
		config.Timeout = 3
	}
	// init backends
	if len(config.Backends) == 0 {
		log.Println("No backends, exiting")
		return
	}
	for x := range config.Backends {
		err := config.Backends[x].Init()
		if err != nil {
			log.Println(err)
			return
		}
	}
	if config.GrpcPort > 0 {
		go config.start_grpc_server()
	}
	if config.HttpPort > 0 {
		go config.start_http_server()
	}
	// wait forever
	c := make(chan struct{})
	<-c
}
