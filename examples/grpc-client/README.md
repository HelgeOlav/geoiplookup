# grpc-client

This client connects to a gRPC server and returns the result from a gRPC Lookup().

Can be used with [sample-l2l](../sample-i2l) that runs as the server.

## Syntax

```shell
grpc-client -server my-server:8888 -timeout 2 1.1.1.1 4.4.4.4 8.8.8.8
```