package main

import (
	pb "bitbucket.org/HelgeOlav/geoiplookup"
	"bitbucket.org/HelgeOlav/utils/version"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"google.golang.org/grpc"
	"time"
)

var (
	connString = flag.String("server", "localhost:8081", "connection to server")
	reqTimeout = flag.Uint("timeout", 3, "timeout in seconds")
)

func init() {
	version.NAME = "grpc-client"
	version.VERSION = pb.Version
}

func main() {
	ver := version.Get()
	flag.Parse()
	conn, err := grpc.Dial(*connString, grpc.WithInsecure())
	if err != nil {
		fmt.Println(ver.LongString())
		fmt.Println(err)
		return
	}
	c := pb.NewGeoIpLookupClient(conn)
	args := flag.Args()
	if len(args) == 0 {
		flag.PrintDefaults()
		return
	}
	for _, key := range args {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(*reqTimeout))
		req := pb.GeoIpRequest{Ip: key}
		resp, err := c.Lookup(ctx, &req)
		if err == nil {
			if resp.Values == nil {
				resp.Values = make(map[string]string)
			}
			resp.Values["lookup_ip"] = key
			bytes, _ := json.Marshal(resp)
			fmt.Println(string(bytes))
		} else {
			fmt.Println(ver.LongString())
			fmt.Println(key, err.Error())
		}
		cancel()
	}
	conn.Close()
}
