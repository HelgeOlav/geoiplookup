package geoiplookup

import "net"

// IsIP returns true if input is an IP address
func IsIP(input string) bool {
	ip := net.ParseIP(input)
	return ip != nil
}
