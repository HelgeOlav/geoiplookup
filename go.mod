module bitbucket.org/HelgeOlav/geoiplookup

go 1.17

require (
	bitbucket.org/HelgeOlav/utils v0.0.0-20230406215710-11738a163115
	github.com/bradfitz/ip2asn v0.0.0-20220725205325-1069e332e707
	github.com/ip2location/ip2location-go/v9 v9.6.0
	github.com/ip2location/ip2proxy-go v3.0.0+incompatible
	google.golang.org/grpc v1.42.0
	google.golang.org/protobuf v1.26.0
	inet.af/netaddr v0.0.0-20220811202034-502d2d690317
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	go4.org/intern v0.0.0-20230205224052-192e9f60865c // indirect
	go4.org/mem v0.0.0-20220726221520-4f986261bf13 // indirect
	go4.org/unsafe/assume-no-moving-gc v0.0.0-20230221090011-e4bae7ad2296 // indirect
	golang.org/x/net v0.0.0-20201021035429-f5854403a974 // indirect
	golang.org/x/sys v0.0.0-20210119212857-b64e53b001e4 // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	lukechampine.com/uint128 v1.3.0 // indirect
)
