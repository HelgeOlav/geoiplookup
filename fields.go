package geoiplookup

import (
	"fmt"
)

const (
	FieldCountryCode = "country_code"
	FieldCity        = "city"
	FieldRegion      = "region"
	FieldGeoLocation = "geolocation"
)

// GetFieldValue returns the content of a field as string, or nil if the field does not exist
func (response *GeoIpResponse) GetFieldValue(field string) *string {
	if response == nil {
		return nil
	}
	switch field {
	case FieldCountryCode:
		return &response.CountryCode
	case FieldCity:
		return response.City
	case FieldRegion:
		return response.Region
	case FieldGeoLocation:
		if response.Geo != nil {
			r := fmt.Sprintf("%f,%f", response.Geo.Latitude, response.Geo.Longitude)
			return &r
		} else {
			return nil
		}
	default:
		r, ok := response.Values[field]
		if ok {
			return &r
		} else {
			return nil
		}
	}
}

// GetFields returns a list of all fields present in the response
func (response *GeoIpResponse) GetFields() []string {
	result := []string{FieldCountryCode}
	// first check the hardcoded fields
	if response.City != nil {
		result = append(result, FieldCity)
	}
	if response.Region != nil {
		result = append(result, FieldRegion)
	}
	if response.Geo != nil {
		result = append(result, FieldGeoLocation)
	}
	// now add the fields from the map
	for x := range response.Values {
		result = append(result, x)
	}
	return result
}
